
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

int parse_time_line(FILE *f, int *x, int *ys, int *ye){
	char buffer[50];
	char *c;

	if(fgets(buffer,50,f) != NULL){
		*x = atoi(buffer);
		c = strchr(buffer,',') + 1;
		
		*ys = atoi(c);
		c = strchr(c,':') + 1;
		*ys = *ys*60 + atoi(c);

		c = strchr(c,',') + 1;
		*ye = atoi(c);
		c = strchr(c,':') + 1;
		*ye = *ye*60 + atoi(c);

		return 1;
	}else{
		return 0;
	}
}

// draws bar at day x for the duration from ys to ye
// x0, y0: upper left corner of graph
// dx: the spacing between days
// dy: the spacing between hours
// x: day
// ys, ye: the times in minutes, there are 1440 minutes per day
void draw_time_bar(FILE *f, float x0, float y0, float dx, float dy, int x, int ys, int ye){
	if(ys<ye){
		fprintf(f,
			"<rect x=\"%f\" y=\"%f\" width=\"%f\" height=\"%f\" />\n",
			x0+dx*(x-0.75), y0+dy*(1440.0-ye)/60.0, dx*0.5, dy*(ye-ys)/60.0);
	}else{
		// current day
		fprintf(f,
			"<rect x=\"%f\" y=\"%f\" width=\"%f\" height=\"%f\" />\n",
			x0+dx*(x-0.75), y0, dx*0.5, dy*(1440.0-ys)/60.0);
		// next day
		fprintf(f,
			"<rect x=\"%f\" y=\"%f\" width=\"%f\" height=\"%f\" />\n",
			x0+dx*(x+0.25), y0+dy*(1440.0-ye)/60.0, dx*0.5, dy*ye/60.0);
	}
}

int main(int argc, char **argv){
	FILE *fin, *fout;

	char buffer[50];

	fin = fopen(argv[1], "r");

	strcpy(buffer,argv[1]);
	strcat(buffer,".svg");
	fout = fopen(buffer,"w");

	// (0,0) is upper left corner

	// document width and height
	float w0, h0;

	// document margins
	float mu, md, ml, mr;

	// graph size
	float w1, h1;

	// other
	int i, j, k;
	int x, ys, ye;
	float dt, dd, t, d, fh;

	// stroke parameters
	float sw;

	// all units should be in pixels
	w0=1000.0; h0=500.0;
	mu=20.0; md=20.0; 
	ml=30.0; mr=20.0;
	sw=0.5;

	w1 = w0-ml-mr;
	h1 = h0-md-mu;

	// document header
	fprintf(fout,
		"<svg width=\"%f\" height=\"%f\" viewBox=\"0 0 %f %f\" xmlns=\"http://www.w3.org/2000/svg\">\n",
		w0, h0, w0, h0);

	// bounding box
	fprintf(fout,
			"<line stroke-width=\"%f\" stroke=\"#000000\" x1=\"%f\" y1=\"%f\" x2=\"%f\" y2=\"%f\" />\n",
			sw, ml, mu, ml, h0-md);
	fprintf(fout,
			"<line stroke-width=\"%f\" stroke=\"#000000\" x1=\"%f\" y1=\"%f\" x2=\"%f\" y2=\"%f\" />\n",
			sw, w0-mr, mu, w0-mr, h0-md);

	fprintf(fout,
			"<line stroke-width=\"%f\" stroke=\"#000000\" x1=\"%f\" y1=\"%f\" x2=\"%f\" y2=\"%f\" />\n",
			sw, ml, mu, w0-mr, mu);
	fprintf(fout,
			"<line stroke-width=\"%f\" stroke=\"#000000\" x1=\"%f\" y1=\"%f\" x2=\"%f\" y2=\"%f\" />\n",
			sw, ml, h0-md, w0-mr, h0-md);

	// calculate spacings
	dt = h1 / 24.0;
	dd = w1 / 31.0;

	fh = dt/2.0;

	// time lines
	t = h0-md;
	for(i=0; i<=24; i++){
		fprintf(fout,
			"<line stroke-width=\"%f\" stroke=\"#000000\" x1=\"%f\" y1=\"%f\" x2=\"%f\" y2=\"%f\" />\n",
			sw*0.25, ml, t, ml+w1, t);
		t -= dt;
	}
	
	// day lines
	d = ml;
	for(i=0; i<=31; i++){
		fprintf(fout,
			"<line stroke-width=\"%f\" stroke=\"#000000\" x1=\"%f\" y1=\"%f\" x2=\"%f\" y2=\"%f\" />\n",
			sw*0.25, d, mu, d, h0-md);
		d += dd;	
	}

	// time labels
	t = h0-md;
	for(i=0; i<24; i++){
		fprintf(fout,
			"<text font-size=\"%f\" x=\"%f\" y=\"%f\" text-anchor=\"middle\">%d</text>\n",
			fh , ml*0.5, t-dt*0.5+fh*0.35, i);
		t -= dt;
	}

	// day labels
	d = ml;
	for(i=1; i<=31; i++){
		fprintf(fout,
			"<text font-size=\"%f\" x=\"%f\" y=\"%f\" text-anchor=\"middle\">%d</text>\n",
			fh , d+dd/2, h0-md*0.5+fh*0.35, i);
		d += dd;	
	}

	/*
	1, 05:00, 13:30
	2, 05:00, 13:00
	3, 05:00, 12:30
	4, 05:00, 11:00
	5, 04:00, 12:30
	6, 05:00, 12:00
	7, 05:00, 11:00
	7, 22:00, 03:00
	8, 20:30, 21:00
	9, 03:30, 10:45
	9, 18:00, 19:00
	*/

	while(parse_time_line(fin,&x,&ys,&ye) != 0){
		draw_time_bar(fout, ml, mu, dd, dt, x, ys, ye);
	}

	// draw_time_bar(fout, ml, mu, dd, dt, 2, 300, 780);
	// draw_time_bar(fout, ml, mu, dd, dt, 3, 300, 750);
	// draw_time_bar(fout, ml, mu, dd, dt, 4, 300, 660);
	// draw_time_bar(fout, ml, mu, dd, dt, 5, 240, 750);
	// draw_time_bar(fout, ml, mu, dd, dt, 6, 300, 720);
	// draw_time_bar(fout, ml, mu, dd, dt, 7, 300, 660);
	// draw_time_bar(fout, ml, mu, dd, dt, 7, 1320, 180);
	// draw_time_bar(fout, ml, mu, dd, dt, 8, 1230, 1260);
	// draw_time_bar(fout, ml, mu, dd, dt, 9, 210, 645);
	// draw_time_bar(fout, ml, mu, dd, dt, 9, 1080, 1140);

	
	// tail
	fprintf(fout,"</svg>\n");

	fclose(fin);
	fclose(fout);
}