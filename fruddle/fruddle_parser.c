
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

int main(int argc, char **argv){
	char *titles, *disc;
	// char *disc, *date , *input, *format;
	char *artist, *title, *album; 
	char buffer[1000];
	bool first;
	int track, time;
	FILE *f, *f2;

	if(argc <= 2){
		printf("./fruddle input.file output.file\n");
		return 0;
	}

	titles = argv[1];
	disc = argv[2];

	f = fopen(titles,"r");
	f2 = fopen(disc,"w");

	fprintf(f2, "#!/bin/bash\n");

	fprintf(f2, "INPUT=\"relax_video_game_music_for_3_hours_voln.webm\"\n");
	fprintf(f2, "OUTPUT=\"%s\"\n", disc);
	fprintf(f2, "ALBUMARTIST=\"Fruddle\"\n");
	fprintf(f2, "DISC=\"%s\"\n", disc);
	fprintf(f2, "DATE=\"2017\"\n");

	fprintf(f2, "# CODEC=\"-c copy\"\n");
	fprintf(f2, "# FORMAT=\"opus\"\n");
	fprintf(f2, "CODEC=\"-codec:a libmp3lame -q:a 4\"\n");
	fprintf(f2, "FORMAT=\"mp3\"\n");

	fprintf(f2, "\n");

	first=true; track=1; time=0;
	while(fgets(buffer,1000,f) != NULL){
		char *s, *n;
		int t;

		s = strstr(buffer,"&amp;t=");
		if(!s){
			continue;
		}

		t = atoi(s+7);
		n = strchr(buffer, '\n');
		artist = strstr(buffer, " -- ") + 4;
		title = strstr(buffer, " - ") + 3;
		album = strstr(buffer, "</a> ") + 5;

		if(!first){
			fprintf(f2, "-t %d ",t-time);
			fprintf(f2, "\"${OUTPUT}_%d.${FORMAT}\"\n",track);
			track++;
		}
		
		printf("%d\n",t);
		printf("\"%.*s\"\n", (int)(artist-title)-4, title);
		printf("\"%.*s\"\n", (int)(n-artist), artist);
		printf("\"%.*s\"\n\n", (int)(title-album)-3, album);

		fprintf(f2, "ffmpeg -ss %d ",t);
		
		fprintf(f2, "-i \"$INPUT\" ");
		fprintf(f2, "-metadata album_artist=\"$ALBUMARTIST\" ");
		fprintf(f2, "-metadata disc=\"$DISC\" ");
		fprintf(f2, "-metadata date=\"$DATE\" ");

		fprintf(f2, "-metadata track=\"%d\" ", track);
		fprintf(f2, "-metadata title=\"%.*s\" ", (int)(artist-title)-4, title);
		fprintf(f2, "-metadata artist=\"%.*s\" ", (int)(n-artist), artist);
		fprintf(f2, "-metadata album=\"%.*s\" ", (int)(title-album)-3, album);
		
		fprintf(f2, "$CODEC ");

		time=t;

		if(first){
			first = false;
		}
	}

	fprintf(f2, "\"${OUTPUT}_%d.${FORMAT}\"\n",track);
	
	fclose(f);
	fclose(f2);
}