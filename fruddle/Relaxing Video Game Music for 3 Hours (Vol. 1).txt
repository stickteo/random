3 hour compilation of chill VGM.
----------
Playlist:
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=0s">0:00</a> Child of Light - Aurora's Theme -- Béatrice Martin
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=210s">3:30</a> Legend of Kartia - REBUS (Holy Woman) -- Kenichi Tsuchiya, Masaki Kurokawa
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=325s">5:25</a> Radical Dreamers - Days of Summer -- Yasunori Mitsuda
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=479s">7:59</a> Lost Odyssey - Great Ruins of the East -- Nobuo Uematsu
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=619s">10:19</a> Final Fantasy Crystal Chronicles - Dreaming of Twilight -- Kumi Tanioka
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=853s">14:13</a> Pikmin - The Forest of Hope -- Hajime Wakai
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=1195s">19:55</a> Rayman Legends - When the Wind Blows -- Christophe Héral, Billy Martin
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=1383s">23:03</a> The Legend of Zelda: The Wind Waker - Forest Haven -- Kenta Nagata, Hajime Wakai, Toru Minegishi, Koji Kondo
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=1517s">25:17</a> Tales of Vesperia - Serenade of the Morning Star -- Motoi Sakuraba, Hibiki Aoyama
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=1824s">30:24</a> Final Fantasy X - Spiran Scenery -- Nobuo Uematsu
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=1994s">33:14</a> Mother - Mother Earth -- Keiichi Suzuki, Hirokazu Tanaka
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=2223s">37:03</a> Animal Crossing - 5PM -- Kazumi Totaka
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=2364s">39:24</a> Kirby's Return to Dream Land - Nutty Noon -- Jun Ishikawa, Hirokazu Ando
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=2569s">42:49</a> Halo - A Walk in the Woods -- Martin O'Donnell, Michael Salvatori
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=2683s">44:43</a> Aquaria - The Traveller -- Alec Holowka
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=2903s">48:23</a> The Last Story - Timbre of the City -- Nobuo Uematsu
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=3167s">52:47</a> Nier - His Dream -- Keiichi Okabe, Kakeru Ishihama, Keigo Hoashi, Takafumi Nishimura
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=3285s">54:45</a> The Elder Scrolls III: Morrowind - Peaceful Waters -- Jeremy Soule
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=3475s">57:55</a> Primal - Jen Meets Arella -- Andrew Barnabas, Paul Arnold
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=3769s">1:02:49</a> Fable - Summer Fields -- Russell Shaw
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=4011s">1:06:51</a> Blue Dragon - Ruins -- Nobuo Uematsu
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=4244s">1:10:44</a> Dark Cloud - Norune Village -- Tomohito Nishiura
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=4402s">1:13:22</a> Metal Gear Solid 3 - Old Metal Gear -- 'Starry.K'
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=4671s">1:17:51</a> Donkey Kong Country: Tropical Freeze - Deep Keep -- David Wise
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=4952s">1:22:32</a> A Boy and His Blob - Subterra -- Daniel Sadowski
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=5117s">1:25:17</a> Xenoblade Chronicles - Satorl, the Shimmering Marsh / Night -- Manami Kiyota
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=5318s">1:28:38</a> Final Fantasy VI - Awakening -- Nobuo Uematsu
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=5463s">1:31:03</a> Okami - Hanasaki Valley -- Masami Ueda, Hiroshi Yamaguchi
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=5589s">1:33:09</a> Chrono Trigger - Corridors of Time -- Yasunori Mitsuda
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=5773s">1:36:13</a> Radiant Historia - The Melody Connecting the World -- Yoko Shimomura
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=5880s">1:38:00</a> Xenosaga Episode II - Old Militia -- Yuki Kajiura, Shinji Hosoe
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=6089s">1:41:29</a> Gitaroo Man - The Legendary Theme (Acoustic Version) -- COIL
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=6248s">1:44:08</a> Final Fantasy X - Thunder Plains -- Masashi Hamauzu
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=6474s">1:47:54</a> Irisu Syndrome! - Usagi Note -- Takeaki Watanabe
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=6723s">1:52:03</a> The Legend of Zelda: Ocarina of Time - Zora's Domain -- Koji Kondo
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=6897s">1:54:57</a> Donkey Kong Country 2 - In A Snow-Bound Land -- David Wise
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=7095s">1:58:15</a> Alundra - The Use of Meia -- Kohei Tanaka
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=7342s">2:02:22</a> Trine - Astral Academy -- Ari Pulkkinen
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=7589s">2:06:29</a> The Legend of Zelda: Spirit Tracks - Papuchia Village -- Toru Minegishi, Manaka Tominaga
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=7732s">2:08:52</a> Doshin the Giant - Yellow Giant -- Tatsuhiko Asano
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=7891s">2:11:31</a> Ecco the Dolphin [Sega CD] - Home Bay -- Spencer Nilsen
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=8105s">2:15:05</a> Opoona - The Village Without Memories -- Noriyuki Kamikura
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=8347s">2:19:07</a> Kingdom Hearts II - The Afternoon Streets -- Yoko Shimomura
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=8526s">2:22:06</a> The Legend of Zelda: Twilight Princess - Hyrule Field (Night) -- Toru Minegishi, Asuka Ohta, Koji Kondo
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=8641s">2:24:01</a> Machinarium - The Glasshouse With Butterfly -- Tomas Dvorak
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=8852s">2:27:32</a> The 3rd Birthday - Angel's Time -- Tsuyoshi Sekito
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=8970s">2:29:30</a> Mass Effect - Uncharted Worlds -- Jack Wall, Sam Hulick
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=9175s">2:32:55</a> Infinite Undiscovery - Forbidden Ground -- Motoi Sakuraba
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=9411s">2:36:51</a> Final Fantasy VIII - Balamb Garden -- Nobuo Uematsu
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=9625s">2:40:25</a> Etrian Odyssey Untold - The Withered Forest -- Yuzo Koshiro
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=9889s">2:44:49</a> Heimdall 2 - To Die -- Martin Iveson
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=10073s">2:47:53</a> The Elder Scrolls IV: Oblivion - King and Country -- Jeremy Soule
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=10319s">2:51:59</a> Pilotwings 64 - Birdman -- Dan Hess
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=10537s">2:55:37</a> Donkey Kong Country: Tropical Freeze - Current Capers ~ Aquatic Ambiance -- David Wise
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="/watch?v=XC2bdmc8_04&amp;t=10831s">3:00:31</a> Final Fantasy IX - Esto Gaza -- Nobuo Uematsu
----------

Relaxing VGM Playlist: <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/playlist?list=PLiU36l-VAX_6sofGTOxMyRfLiwrjQCGck">https://www.youtube.com/playlist?list...</a>
 
Sleepy VGM Playlist: <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/playlist?list=PLiU36l-VAX_6Ln_mWndTtT56JNvMCZA8h">https://www.youtube.com/playlist?list...</a>